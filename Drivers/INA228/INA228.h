#ifndef __INA228_H__
#define __INA228_H__
#include <stdbool.h>
#include <stdint.h>
#include "stm32g4xx_hal.h"
extern I2C_HandleTypeDef hi2c1;

#define I2C_HANDLE_INA228 hi2c1
#define INA228_ADDR 0x40

#define INA228_CONFIG 0x00
#define INA228_ADC_CONFIG 0x01
#define INA228_SHUNT_CAL 0x02
#define INA228_SHUNT_TEMPCO 0x03
#define INA228_VSHUNT 0x04
#define INA228_VBUS 0x05
#define INA228_DIETEMP 0x06
#define INA228_CURRENT 0x07
#define INA228_POWER 0x08
#define INA228_ENERGY 0x09
#define INA228_CHARGE 0x0A
#define INA228_DIAG_ALRT 0x0B
#define INA228_SOVL 0x0C
#define INA228_SUVL 0x0D
#define INA228_BOVL 0x0E
#define INA228_BUVL 0x0F
#define INA228_TEMP_LIMIT 0x10
#define INA228_PWR_LIMIT 0x11
#define INA228_MANUFACTURER_ID 0x3E
#define INA228_DEVICE_ID 0x3F

typedef struct
{
    uint8_t RST;
    uint8_t RSTACC;
    uint8_t CONVDLY;
    uint8_t TEMPCOMP;
    uint8_t ADCRANGE;
    float MEC; // MaximumExpectedCurrent
} INA228_CONFIGTypeDef;

typedef struct
{
    uint8_t MODE;
    uint8_t VBUSCT;
    uint8_t VSHCT;
    uint8_t VTCT;
    uint8_t AVG;
} INA228_ADCTypeDef;

typedef struct
{
    uint8_t ALATCH;
    uint8_t CNVR;
    uint8_t SLOWALERT;
    uint8_t APOL;
    uint8_t ENERGYOF; // Read Only
    uint8_t CHARGEOF; // Read Only
    uint8_t MATHOF;   // Read Only
    uint8_t RESERVE;  // Read Only
    uint8_t TMPOL;
    uint8_t SHNTOL;
    uint8_t SHNTUL;
    uint8_t BUSOL;
    uint8_t BUSUL;
    uint8_t POL;
    uint8_t CNVRF;
    uint8_t MEMSTAT;

} INA228_ALRETTypeDef;

uint8_t INA228_Init(INA228_CONFIGTypeDef *INA228_Init);
void INA228_ADC_Config(INA228_ADCTypeDef *INA228_ADCInit);
void INA228_SET_SHUNT_CAL(uint16_t RSHUNT, uint16_t ppm);
float INA228_Get_VSHUNT(void);
float INA228_Get_VBUS(void);
float INA228_Get_DIETEMP(void);
float INA228_Get_CURRENT(void);
float INA228_Get_POWER(void);
float INA228_Get_ENERGY(void);
float INA228_Get_CHARGE(void);
void INA228_RESET_ACC(void);
void INA228_RESET(void);
void INA228_Write_Mem(uint8_t addr, uint8_t LENGTH, uint8_t *buff);
void INA228_Read_Mem(uint8_t addr, uint8_t LENGTH, uint8_t *buff);

#endif