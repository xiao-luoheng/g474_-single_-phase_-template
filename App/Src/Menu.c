#include "Menu.h"

extern float I_ref;
extern comkey_t key0,key1,key2;

float Io = 2,K = 1;
int selection = 0, configuring = 0;

void Menu_Init()
{
    #ifdef INVERTER_1
    OLED_ShowString(16, 4, "Io:", 8);
    OLED_ShowString(16, 6, "K:", 8);
    OLED_Show_float(48, 4, Io);
    OLED_Show_float(48, 6, K);
    #endif
    #ifdef INVERTER_2
    OLED_ShowString(16, 0, "Io:", 8);
    OLED_ShowString(16, 2, "K:", 8);
    OLED_Show_float(48, 0, Io);
    OLED_Show_float(48, 2, K);
    #endif
}

void Menu_Refresh()
{
    #ifdef INVERTER_1
    OLED_Show_float(48, 4, Io);
    OLED_Show_float(48, 6, K);
    #endif
    #ifdef INVERTER_2
    OLED_Show_float(48, 0, Io);
    OLED_Show_float(48, 2, K);
    #endif
}



void Menu_Callback(int key)
{
    if(key == 0)
    {
        if(configuring)
        {
            #ifdef INVERTER_1
            I_ref = K*Io/(K+1);
            #endif
            #ifdef INVERTER_2
            I_ref = Io/(K+1);
            #endif            
            configuring = 0;
        }    
        else
            configuring = 1;      
    }
    if(key == 1)
    {
        if(configuring)
        {
            switch (selection)
            {
                case 0:
                    Io += (Io == 4)?0:0.05;
                break;
                case 1:
                    K += (K == 2)?0:0.1;
                break;
            }            
        }    
        else
            selection = (selection+1)%SELECTION_SUM;        
    }
    if(key == 2)
    {
        if(configuring)
        {
            switch (selection)
            {
                case 0:
                    Io -= (Io == 1)?0:0.05;
                break;
                case 1:
                    K -= (K == 0.5)?0:0.1;
                break;
            }            
        }    
        else
            selection = (selection == 0)?(SELECTION_SUM-1):(selection-1);  
    }
    Menu_Refresh();
}