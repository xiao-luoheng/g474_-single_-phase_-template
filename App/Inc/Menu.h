#ifndef MENU
#define MENU

#include "PFC.h"
#include "comKey.h"

#define SELECTION_SUM 2

void Menu_Init();
void Menu_Refresh();
void Menu_Callback(int key);

#endif